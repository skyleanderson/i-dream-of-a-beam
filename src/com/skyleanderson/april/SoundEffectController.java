package com.skyleanderson.april;

import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Sound;

public class SoundEffectController
{
	public static final int   DEFAULT_BPM                  = 120;

	public static final int   BEATS_PER_FRAME              = 16;

	public static final float HIGHER_FIFTH_FREQUENCY_RATIO = 1.4983f;
	public static final float LOWER_FIFTH_FREQUENCY_RATIO  = 0.66742f;

	public SoundEffectController( )
	{}

	protected Sound   blipSound, missSound, deathSound;
	protected int     bpm              = DEFAULT_BPM;
	protected float   millisToBeat     = 60000 / DEFAULT_BPM;
	protected int     elapsedMillis    = 0;
	protected int     quarterBeats     = 0;
	protected int     beats            = 0;

	protected int     blipsToPlay      = 0;

	protected boolean firstUpdate      = true;

	protected boolean onBeat           = false;
	protected int     framesDone       = 0;

	// music
	// track list:
	// 0 - drums
	// 1 - avatar
	// 2 - tones
	// 3 - bass
	// 4 - harp
	protected Sound[] track            = new Sound[5];
	protected float[] trackProbability = { .95f, .6f, 0, .4f, .2f };
	protected int[]   trackPlayCount   = new int[5];
	protected boolean playMusic        = true;

	public void init( GameContainer gc, Game game )
	{
		try
		{
			blipSound = new Sound("res/blip2.wav");
			missSound = new Sound("res/miss.wav");
			deathSound = new Sound("res/death.wav");

			track[0] = new Sound("res/drum.wav");
			track[1] = new Sound("res/avatar.wav");
			track[2] = new Sound("res/tones.wav");
			track[3] = new Sound("res/bass.wav");
			track[4] = new Sound("res/harp.wav");
		} catch (Exception ex)
		{
			ex.getStackTrace();
		}
	}

	public void update( GameContainer gc, Game game, int delta )
	{
		if (!playMusic) return;

		if (firstUpdate)
		{
			track[0].play();
			firstUpdate = false;
		}

		onBeat = false;
		elapsedMillis += delta;
		if (elapsedMillis > millisToQuarterBeat())
		{
			elapsedMillis -= millisToQuarterBeat();
			quarterBeats++;

			// perform quarter beat

			if (quarterBeats % 2 == 0)
			{
				double random = Math.random();
				switch (blipsToPlay)
				{
					case 1:
						if (random < .3333)
							blipSound.play(LOWER_FIFTH_FREQUENCY_RATIO, 1);
						else if (random < .66667)
							blipSound.play(HIGHER_FIFTH_FREQUENCY_RATIO, 1);
						else
							blipSound.play(1, 1);
						break;
					case 2:
						if (random < .3333)
						{
							blipSound.play(LOWER_FIFTH_FREQUENCY_RATIO, 1);
							blipSound.play(1, 1);
						} else if (random < .66667)
						{
							blipSound.play(HIGHER_FIFTH_FREQUENCY_RATIO, 1);
							blipSound.play(LOWER_FIFTH_FREQUENCY_RATIO, 1);
						} else
						{
							blipSound.play(1, 1);
							blipSound.play(LOWER_FIFTH_FREQUENCY_RATIO, 1);
						}
						break;
					case 3:
						blipSound.play(1, 1);
						blipSound.play(HIGHER_FIFTH_FREQUENCY_RATIO, 1);
						blipSound.play(LOWER_FIFTH_FREQUENCY_RATIO, 1);
						break;
				}
				blipsToPlay = 0;

				if (_playMiss)
				{
					_playMiss = false;
					missSound.play();
				}

				if (_playDeath)
				{
					_playDeath = false;
					deathSound.play(HIGHER_FIFTH_FREQUENCY_RATIO, 1.0f);
				}
				// perform half beat
			}

			if (quarterBeats == 4)
			{
				// perform beat
				beats++;
				quarterBeats = 0;
				onBeat = true;
			}

			if (beats == BEATS_PER_FRAME)
			{
				// handle frame
				// randomly decide if each track will be played this frame
				double r;
				boolean somethingPlaying = false;
				for (int i = 1; i < 5; i++)
				{
					if (trackPlayCount[i] > 0)
					{
						track[i].play();
						trackPlayCount[i]--;
						somethingPlaying = true;
					} else
					{
						r = Math.random();
						if (r < trackProbability[i])
						{
							trackPlayCount[i] = (int) (Math.random() * 4);
							track[i].play();
							somethingPlaying = true;
						}
					}
				}

				if (!somethingPlaying)
				{
					trackPlayCount[0] = (int) (Math.random() * 3) + 1;
					track[0].play();
				} else
				{
					if (trackPlayCount[0] > 0)
					{
						track[0].play();
						trackPlayCount[0]--;
					} else
					{
						r = Math.random();
						if (r < trackProbability[0])
						{
							trackPlayCount[0] = (int) (Math.random() * 4);
							track[0].play();
						}
					}
				}
				beats = 0;
				framesDone++;
			}

		}
	}

	public void setBPM( int bpm )
	{
		this.bpm = bpm;
		this.millisToBeat = 60000 / bpm;
	}

	protected float millisToHalfBeat( )
	{
		return .5f * millisToBeat;
	}

	protected float millisToQuarterBeat( )
	{
		return .25f * millisToBeat;
	}

	public void playBlip( )
	{
		if (blipsToPlay < 3) blipsToPlay++;
	}

	public void stopAll( )
	{
		for (int i = 0; i < 5; i++)
			track[i].stop();

		playMusic = false;
	}

	public void resume( )
	{
		this.stopAll(); // safety check
		playMusic = true;
		firstUpdate = true;
		elapsedMillis = 0;
		quarterBeats = 0;
		beats = 0;
	}

	public boolean _playMiss = false;

	public void playMiss( )
	{
		_playMiss = true;
	}

	public boolean _playDeath = false;

	public void playDeath( )
	{
		_playDeath = true;
	}

	public boolean isOnBeat( )
	{
		return onBeat;
	}

	public int getFramesDone( )
	{
		return framesDone;
	}

	public void clearFrames( int amount )
	{
		framesDone -= amount;
	}
}
