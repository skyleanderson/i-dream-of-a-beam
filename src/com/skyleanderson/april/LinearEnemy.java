package com.skyleanderson.april;

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class LinearEnemy extends Enemy
{

	public LinearEnemy( )
	{}

	public LinearEnemy( Color c, float width, float height )
	{
		super(c, width, height);
	}

	public LinearEnemy( ArrayList<Animation> animations )
	{
		super(animations);
	}

	public LinearEnemy( Animation animation )
	{
		super(animation);
	}

	public LinearEnemy( Color c, float width, float height, ArrayList<Animation> animations )
	{
		super(c, width, height, animations);
	}

	public LinearEnemy( Color c, float width, float height, Animation animation )
	{
		super(c, width, height, animation);
	}

	@Override
	public void render( GameContainer gc, Game game, Graphics g, float offsetX, float offsetY )
	{
		if (!killed)
		{
			g.setColor(this.boundingBox.color);
			g.fillRect(this.left(), this.top(), this.right() - this.left(), this.bottom() - this.top());

		}
	}

	@Override
	public void update( )
	{
		if (killed) return;

		super.update();

		if (position.x < 0)
		{
			position.x = 0;
			this.velocity.x *= -1;
		} else if (position.x > LD26.GAME_WIDTH - this.boundingBox.getWidth())
		{
			position.x = LD26.GAME_WIDTH - this.boundingBox.getWidth();
			this.velocity.x *= -1;
		}

		if (position.y < 0)
		{
			position.y = 0;
			this.velocity.y *= -1;
		} else if (position.y > LD26.GAME_HEIGHT - this.boundingBox.getHeight())
		{
			position.y = LD26.GAME_HEIGHT - this.boundingBox.getHeight();
			this.velocity.y *= -1;
		}
	}

}
