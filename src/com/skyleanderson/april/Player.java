package com.skyleanderson.april;

import java.util.ArrayList;
import java.util.LinkedList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import com.skyleanderson.april.states.GamePlayState;
import com.skyleanderson.april.util.Prop;

public class Player extends Prop
{

	// /////////////////////////////////////////////////////////////////////
	// CONSTANTS ///////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////
	public static final float SPEED           = 4.2f;
	public static final int   ATTACK_COOLDOWN = 10;                    // in frames
	public static final int   ATTACK_MARGIN   = 8;
	public static final Color DEFAULT_COLOR   = new Color(255, 210, 0);

	// /////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS ////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////
	public Player( )
	{}

	public Player( Color c, float width, float height )
	{
		super(c, width, height);
	}

	public Player( ArrayList<Animation> animations )
	{
		super(animations);
	}

	public Player( Animation animation )
	{
		super(animation);
	}

	public Player( Color c, float width, float height, ArrayList<Animation> animations )
	{
		super(c, width, height, animations);
	}

	public Player( Color c, float width, float height, Animation animation )
	{
		super(c, width, height, animation);
	}

	// /////////////////////////////////////////////////////////////////////
	// MEMBER VARIABLES ////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////
	protected Color                 _color            = DEFAULT_COLOR;
	// protected Color _color = new Color(255, 130, 0);
	protected Prop                  _attackBarLeft    = new Prop(_color, 16, 16);
	protected Prop                  _attackBarRight   = new Prop(_color, 16, 16);
	protected Prop                  _attackBarUp      = new Prop(_color, 16, 16);
	protected Prop                  _attackBarDown    = new Prop(_color, 16, 16);

	protected float                 _attackAlpha      = 1.0f;
	protected Color                 _attackAlphaColor = new Color(0, 0, 0, 1);

	protected LinkedList<Enemy>     _enemiesKilled    = new LinkedList<Enemy>();
	protected LinkedList<Enemy>     _allEnemies;                                   // = new LinkedList<Prop>();

	protected ParticleFactory       _particleFactory  = new ParticleFactory();
	protected LinkedList<Particle>  _activeParticles  = new LinkedList<Particle>();
	protected LinkedList<Particle>  _removeParticles  = new LinkedList<Particle>();
	protected LinkedList<CallOut>   _activeCallOuts   = new LinkedList<CallOut>();
	protected LinkedList<CallOut>   _removeCallOuts   = new LinkedList<CallOut>();

	protected Vector2f              _tempVector       = new Vector2f();

	protected SoundEffectController _soundController  = null;

	protected boolean               _isDead           = false;

	protected GamePlayState         _gameState        = null;
	protected UnicodeFont           _fontSmall;

	// /////////////////////////////////////////////////////////////////////
	// GAME METHODS ////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////
	public void init( GameContainer gc, Game game )
	{
		try
		{
			_fontSmall = new UnicodeFont("res/LILLIPUT.TTF", 16, true, false);
			_fontSmall.addAsciiGlyphs();
			_fontSmall.getEffects().add(new ColorEffect());
			_fontSmall.loadGlyphs();
		} catch (Exception ex)
		{
			ex.getStackTrace();
		}

		// not sure if i'll need this.... probably for sounds...
		_isDead = false;
		this.boundingBox.fill = true;
		this.setOffGround();
		_gameState = (GamePlayState) ((StateBasedGame) game).getState(GamePlayState.ID);

	}

	@Override
	public void render( GameContainer gc, Game game, Graphics g, float offsetX, float offsetY )
	{
		// ^^ probably don't need the offsets

		// draws a colored rectangle
		if (!this.isDead()) super.render(gc, game, g, offsetX, offsetY);

		if (_attackCoolDown > 0)
		{
			_attackAlphaColor.r = _attackBarLeft.boundingBox.color.r;
			_attackAlphaColor.g = _attackBarLeft.boundingBox.color.g;
			_attackAlphaColor.b = _attackBarLeft.boundingBox.color.b;
			_attackAlphaColor.a = _attackAlpha;
			g.setColor(_attackAlphaColor);

			g.fillRect(this.left() - ATTACK_MARGIN, this.top() - ATTACK_MARGIN, this.boundingBox.getWidth() + 2 * ATTACK_MARGIN, this.boundingBox.getHeight()
			        + 2 * ATTACK_MARGIN);

			if (_doAttackHorizontal)
			{
				g.fillRect(_attackBarLeft.left(), _attackBarLeft.top(), _attackBarLeft.boundingBox.getWidth(), _attackBarLeft.boundingBox.getHeight());
				g.fillRect(_attackBarRight.left(), _attackBarRight.top(), _attackBarRight.boundingBox.getWidth(), _attackBarRight.boundingBox.getHeight());
				g.setColor(_attackBarLeft.boundingBox.color);
				g.fillRect(_attackBarLeft.left(), _attackBarLeft.top() + 4, _attackBarLeft.boundingBox.getWidth(), _attackBarLeft.boundingBox.getHeight() - 8);
				g.fillRect(_attackBarRight.left(), _attackBarRight.top() + 4, _attackBarRight.boundingBox.getWidth(),
				        _attackBarRight.boundingBox.getHeight() - 8);
				// _attackBarLeft.render(gc, game, g, 0, 0);
				// _attackBarRight.render(gc, game, g, 0, 0);
			} else if (_doAttackVertical)
			{
				g.fillRect(_attackBarUp.left(), _attackBarUp.top(), _attackBarUp.boundingBox.getWidth(), _attackBarUp.boundingBox.getHeight());
				g.fillRect(_attackBarDown.left(), _attackBarDown.top(), _attackBarDown.boundingBox.getWidth(), _attackBarDown.boundingBox.getHeight());
				g.setColor(_attackBarLeft.boundingBox.color);
				g.fillRect(_attackBarUp.left() + 4, _attackBarUp.top(), _attackBarUp.boundingBox.getWidth() - 8, _attackBarUp.boundingBox.getHeight());
				g.fillRect(_attackBarDown.left() + 4, _attackBarDown.top(), _attackBarDown.boundingBox.getWidth() - 8, _attackBarDown.boundingBox.getHeight());

			}
		}

		// particles
		for (Particle p : _activeParticles)
		{
			p.render(gc, game, g, 0, 0);
		}

		for (CallOut co : _activeCallOuts)
			co.render(gc, game, g, 0, 0);
	}

	@Override
	public void update( )
	{

		if (!this.isDead())
		{
			// handle input
			// movement
			if (_attackCoolDown <= 0)
			{
				this.velocity.set(0, 0);
				if (_doRight)
					this.velocity.x = 1;
				else if (_doLeft) this.velocity.x = -1;

				if (_doUp)
					this.velocity.y = -1;
				else if (_doDown) this.velocity.y = 1;
				this.velocity.normalise();
				this.velocity.scale(SPEED);

				position.add(velocity);
				if (position.x < 0)
					position.x = 0;
				else if (position.x > LD26.GAME_WIDTH - this.boundingBox.getWidth()) position.x = LD26.GAME_WIDTH - this.boundingBox.getWidth();

				if (position.y < 0)
					position.y = 0;
				else if (position.y > LD26.GAME_HEIGHT - this.boundingBox.getHeight()) position.y = LD26.GAME_HEIGHT - this.boundingBox.getHeight();
			}

			// attack
			// initialize attack bars
			if (_doAttackHorizontal && _attackCoolDown <= 0)
			{
				_attackCoolDown = ATTACK_COOLDOWN;

				_attackBarLeft.setPosition(0, this.top());
				_attackBarLeft.boundingBox.setWidth(this.left() - ATTACK_MARGIN);

				_attackBarRight.setPosition(this.right() + ATTACK_MARGIN, this.top());
				_attackBarRight.boundingBox.setWidth(LD26.GAME_WIDTH - (this.right() + ATTACK_MARGIN));
			} else if (_doAttackVertical && _attackCoolDown <= 0)
			{
				_attackCoolDown = ATTACK_COOLDOWN;

				_attackBarUp.setPosition(this.left(), 0);
				_attackBarUp.boundingBox.setHeight(this.top() - ATTACK_MARGIN);

				_attackBarDown.setPosition(this.left(), this.bottom() + ATTACK_MARGIN);
				_attackBarDown.boundingBox.setHeight(LD26.GAME_HEIGHT - (this.bottom() + ATTACK_MARGIN));
			}
		}
		// attack cooldown and check for kills
		int prevCooldown = _attackCoolDown;
		_attackCoolDown--;
		if (!isDead())
		{
			if (_attackCoolDown > 0)
			{
				_attackAlpha = (.5f * ((float) (_attackCoolDown) / (float) (ATTACK_COOLDOWN))) + .5f;
				_attackAlphaColor.a = _attackAlpha;

				// check for collisions
				if (_allEnemies != null)
				{
					if (_doAttackHorizontal)
						checkForEnemyKills(_attackBarLeft, _attackBarRight);
					else if (_doAttackVertical)

					checkForEnemyKills(_attackBarUp, _attackBarDown);

				}
			} else if (_attackCoolDown == 0 && prevCooldown != 0)
			{
				// check for missed shot
				if (_enemiesKilled.size() == 0)
				{
					_soundController.playMiss();
					_gameState.breakCombo();
				} else
					_gameState.scoreKills(_enemiesKilled.size());
			}

			// check for death
			for (Enemy e : _allEnemies)
			{
				if (!e.isKilled()) if (e.checkSingleCollision(this))
				{
					this.die();
				}
			}
		}
		if (_attackCoolDown <= 0)
		// not attacking
		{
			_doAttackHorizontal = false;
			_doAttackVertical = false;
			_enemiesKilled.clear();
		}

		// update particles
		_removeParticles.clear();
		for (Particle p : _activeParticles)
		{
			p.update();
			if (!p.isActive()) _removeParticles.add(p);
		}
		for (Particle p : _removeParticles)
			_activeParticles.remove(p);

		// update callouts
		_removeCallOuts.clear();
		for (CallOut co : _activeCallOuts)
		{
			co.update();
			if (!co.isActive()) _removeCallOuts.add(co);
		}
		for (CallOut co : _removeCallOuts)
			_activeCallOuts.remove(co);
	}

	// /////////////////////////////////////////////////////////////////////
	// HELPERS /////////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////
	public void setEnemyList( LinkedList<Enemy> enemies )
	{
		this._allEnemies = enemies;
	}

	private void spawnParticles( Prop e )
	{
		// spawn death particles
		for (int i = 0; i < 20; i++)
		{
			Particle p = _particleFactory.getNextParticle();
			p.init();
			p.setColor(e.boundingBox.color);
			p.setPosition(e.getPosition());
			p.setNewRandomSize(10, 50);
			_tempVector.set(1, 0);
			_tempVector.add(360 * Math.random());
			_tempVector.scale(Particle.DEFAULT_SPEED);
			p.setVelocity(_tempVector);
			_tempVector.scale(-.01f * (float) Math.random());
			p.setAcceleration(_tempVector);
			_activeParticles.add(p);
		}
	}

	public void setSoundEffectController( SoundEffectController controller )
	{
		this._soundController = controller;
	}

	private void checkForEnemyKills( Prop attack1, Prop attack2 )
	{
		for (Enemy enemy : _allEnemies)
		{
			if (!enemy.isKilled())
			{
				if (attack1.checkSingleCollision(enemy) || attack2.checkSingleCollision(enemy))
				{
					enemy.kill();
					_enemiesKilled.add(enemy);
					spawnParticles(enemy);
					_soundController.playBlip();

					if (_enemiesKilled.size() > 1)
					{
						CallOut co = new CallOut(Color.red, 16, 16);
						co.init();
						co.setFont(_fontSmall);
						co.setColor(enemy.boundingBox.color);
						co.setPosition(enemy.getPosition());
						co.setAmount(_enemiesKilled.size());
						_activeCallOuts.add(co);
					}
				}
			}
		}
	}

	public boolean isDead( )
	{
		return _isDead;
	}

	public boolean readyToContinue( )
	{
		// wait until all graphical effects are done
		return ((_attackCoolDown <= 0) && (_activeParticles.size() == 0));
	}

	public void die( )
	{
		// die
		_soundController.playDeath();
		this._isDead = true;
		spawnParticles(this);
	}

	// /////////////////////////////////////////////////////////////////////
	// INPUT ///////////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////
	private boolean _doRight            = false;
	private boolean _doLeft             = false;
	private boolean _doUp               = false;
	private boolean _doDown             = false;
	private boolean _doAttackHorizontal = false;
	private boolean _doAttackVertical   = false;
	private int     _attackCoolDown     = 0;

	public void moveRight( )
	{
		this._doRight = true;
	}

	public void moveLeft( )
	{
		this._doLeft = true;
	}

	public void moveUp( )
	{
		this._doUp = true;
	}

	public void moveDown( )
	{
		this._doDown = true;
	}

	public void stopRight( )
	{
		this._doRight = false;
	}

	public void stopLeft( )
	{
		this._doLeft = false;
	}

	public void stopUp( )
	{
		this._doUp = false;
	}

	public void stopDown( )
	{
		this._doDown = false;
	}

	public void attackHorizontal( )
	{
		this._doAttackHorizontal = true;
	}

	public void attackVertical( )
	{
		this._doAttackVertical = true;
	}

}
