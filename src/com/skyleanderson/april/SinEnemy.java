package com.skyleanderson.april;

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

public class SinEnemy extends Enemy
{
	public static final float PI_OVER_2 = 1.571f;
	public static final float MAX_ANGLE = 30f;

	public static final float SPEED     = 2.9f;

	public SinEnemy( )
	{
		// TODO Auto-generated constructor stub
	}

	public SinEnemy( Color c, float width, float height )
	{
		super(c, width, height);
		// TODO Auto-generated constructor stub
	}

	public SinEnemy( ArrayList<Animation> animations )
	{
		super(animations);
		// TODO Auto-generated constructor stub
	}

	public SinEnemy( Animation animation )
	{
		super(animation);
		// TODO Auto-generated constructor stub
	}

	public SinEnemy( Color c, float width, float height, ArrayList<Animation> animations )
	{
		super(c, width, height, animations);
		// TODO Auto-generated constructor stub
	}

	public SinEnemy( Color c, float width, float height, Animation animation )
	{
		super(c, width, height, animation);
		// TODO Auto-generated constructor stub
	}

	private Vector2f _direction = new Vector2f();
	private boolean  _clockwise = false;
	private float    _rotation  = 0;

	@Override
	public void render( GameContainer gc, Game game, Graphics g, float offsetX, float offsetY )
	{
		if (!killed)
		{
			g.setColor(this.boundingBox.color);
			g.fillRect(this.left(), this.top(), this.right() - this.left(), this.bottom() - this.top());

		}
	}

	@Override
	public void update( )
	{
		if (killed) return;

		if (_clockwise)
			_rotation -= 1;
		else
			_rotation += 1;

		if (_rotation > MAX_ANGLE || _rotation < -MAX_ANGLE) _clockwise = !_clockwise;

		velocity.set(_direction);
		velocity.normalise();
		velocity.add(_rotation);
		velocity.scale(SPEED);

		super.update();

		if (position.x < 0)
		{
			position.x = 0;
			this._direction.x *= -1;
		} else if (position.x > LD26.GAME_WIDTH - this.boundingBox.getWidth())
		{
			position.x = LD26.GAME_WIDTH - this.boundingBox.getWidth();
			this._direction.x *= -1;
		}

		if (position.y < 0)
		{
			position.y = 0;
			this._direction.y *= -1;
		} else if (position.y > LD26.GAME_HEIGHT - this.boundingBox.getHeight())
		{
			position.y = LD26.GAME_HEIGHT - this.boundingBox.getHeight();
			this._direction.y *= -1;
		}
	}

	public void setDirection( float x, float y )
	{
		_direction.set(x, y);
	}
}
