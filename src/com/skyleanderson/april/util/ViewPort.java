package com.skyleanderson.april.util;

public interface ViewPort
{

	public abstract float getLeft( );

	public abstract float getRight( );

	public abstract float getTop( );

	public abstract float getBottom( );

	public abstract void update( );

	/**
	 * Returns the left-most tile index to draw. This will likely be off the left side of the screen.
	 * 
	 * @return
	 */
	public abstract float getTileLeft( );

	/**
	 * This returns the number of pixels that the left-most tile is off the screen
	 * 
	 * @return
	 */
	public abstract float getTileLeftOffset( );

	/**
	 * Returns the right-most tile index to draw. This will likely be off the right side of the screen.
	 * 
	 * @return
	 */
	public abstract float getTileRight( );

	/**
	 * Returns the left-most tile index to draw. This will likely be off the left side of the screen.
	 * 
	 * @return
	 */
	public abstract float getTileTop( );

	/**
	 * This returns the number of pixels that the left-most tile is off the screen
	 * 
	 * @return
	 */
	public abstract float getTileTopOffset( );

	/**
	 * Returns the right-most tile index to draw. This will likely be off the right side of the screen.
	 * 
	 * @return
	 */
	public abstract float getTileBottom( );

	public abstract float getTileWidth( );

	public abstract float getTileHeight( );

}