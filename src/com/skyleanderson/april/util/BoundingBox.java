package com.skyleanderson.april.util;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

/**
 * This represents an axis aligned bounding box.
 * 
 * @author ska
 * 
 */
public class BoundingBox
{
	// protected GameApplet owner;
	protected float width            = 16, height = 16;
	protected float offsetX          = 0, offsetY = 0;
	public Color    color            = Color.green;
	public boolean  fill             = true;
	public Color    transparentColor = null;

	public BoundingBox( )
	{}

	public BoundingBox( float width, float height )
	{
		if (width <= 0 || height <= 0) { throw new IllegalArgumentException("Bounding Box width and height must be > 0."); }
		this.width = width;
		this.height = height;
		this.transparentColor = new Color(this.color.r, this.color.g, this.color.b, .2f);
	}

	public BoundingBox( float width, float height, float x, float y )
	{
		this(width, height);
		offsetX = x;
		offsetY = y;
	}

	public BoundingBox( float width, float height, float x, float y, Color c )
	{
		this(width, height, x, y);
		this.color = c;
		this.transparentColor = new Color(this.color.r, this.color.g, this.color.b, .2f);
	}

	public float getWidth( )
	{
		return width;
	}

	public float getHeight( )
	{
		return height;
	}

	public float getOffsetX( )
	{
		return offsetX;
	}

	public float getOffsetY( )
	{
		return offsetY;
	}

	public void setOffsetX( float offset )
	{
		offsetX = offset;
	}

	public void setOffsetY( float offset )
	{
		offsetY = offset;
	}

	public void setWidth( float width )
	{
		this.width = width;
	}

	public void setHeight( float height )
	{
		this.height = height;
	}

	public void setColor( Color color )
	{
		this.color = color;
		this.transparentColor = new Color(color.r, color.g, color.b, .2f);
	}

	public void setFillOnRender( boolean fill )
	{
		this.fill = fill;
	}

	/**
	 * Draws the bounding box to the given graphics element. Should only be used for debugging.
	 * The default behavior is to draw a solid rectangle around the box that is filled with
	 * translucent color. Set the BoundingBox 'fill' property to false to remove the fill.
	 * 
	 * @param g
	 *            The graphics element to draw to
	 * @param x
	 *            the x position to draw at (+ offsetX)
	 * @param y
	 *            the y position to draw at (+ offsetY)
	 */
	public void render( Graphics g, float x, float y )
	{
		Color old = g.getColor();
		if (this.fill)
		{
			g.setColor(this.transparentColor);
			g.fillRect(x + offsetX, y + offsetY, this.width, this.height);
		}
		g.setColor(this.color);
		g.drawRect(x + offsetX, y + offsetY, this.width, this.height);
		g.setColor(old);
	}
}
