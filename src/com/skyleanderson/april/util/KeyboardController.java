package com.skyleanderson.april.util;
import java.awt.event.*;

public class KeyboardController implements KeyListener{

	public boolean right, left, down, up, grab, blink;
	
	public int keyRight    = KeyEvent.VK_D;
	public int keyLeft     = KeyEvent.VK_A;
	public int keyDown     = KeyEvent.VK_S;
	public int keyUp       = KeyEvent.VK_W;
	public int keyGrab     = KeyEvent.VK_N;
	public int keyBlink    = KeyEvent.VK_M;
	
	public int altKeyRight = KeyEvent.VK_RIGHT;
	public int altKeyLeft  = KeyEvent.VK_LEFT;
	public int altKeyDown  = KeyEvent.VK_DOWN;
	public int altKeyUp    = KeyEvent.VK_UP;
	public int altKeyGrab  = KeyEvent.VK_Z;
	public int altKeyBlink = KeyEvent.VK_X;
	
	
	public void UpdateKeyUp(int keyCode)
	{
		altKeyUp = keyUp;
		keyUp = keyCode;
	}	
	public void UpdateKeyDown(int keyCode)
	{
		altKeyDown = keyDown;
		keyDown = keyCode;
	}	
	public void UpdateKeyLeft(int keyCode)
	{
		altKeyLeft = keyLeft;
		keyLeft = keyCode;
	}
	public void UpdateKeyRight(int keyCode)
	{
		altKeyRight = keyRight;
		keyRight = keyCode;
	}
	public void UpdateKeyGrab(int keyCode)
	{
		altKeyGrab = keyGrab;
		keyGrab = keyCode;
	}
	public void UpdateKeyBlink(int keyCode)
	{
		altKeyBlink = keyBlink;
		keyBlink = keyCode;
	}
	
	public void keyPressed(KeyEvent e)
	{
		int key = e.getKeyCode();	
		
		if ( key == this.keyLeft || key == this.altKeyLeft )
			left = true;
		else if ( key == this.keyRight || key == this.altKeyRight ) 
			right = true;
		else if ( key == this.keyUp || key == this.altKeyUp )
			up = true;
		else if ( key == this.keyDown || key == this.altKeyDown )
			down = true;
		else if ( key == this.keyGrab || key == this.altKeyGrab )
			grab = true;
		else if ( key == this.keyBlink || key == this.altKeyBlink )
			blink = true;
	}	
	
	public void keyReleased(KeyEvent e)
	{
		int key = e.getKeyCode();
		
		if ( key == this.keyLeft || key == this.altKeyLeft )
			left = false;
		else if ( key == this.keyRight || key == this.altKeyRight )
			right = false;
		else if ( key == this.keyUp || key == this.altKeyUp )
			up = false; 
		else if ( key == this.keyDown || key == this.altKeyDown )
			down = false;
		else if ( key == this.keyGrab || key == this.altKeyGrab )
			grab = false;
		else if ( key == this.keyBlink || key == this.altKeyBlink )
			blink = false;
	}
	
    //not used
    public void keyTyped( KeyEvent e ) { }
    
	public boolean Up()    { return up; }
	public boolean Down()  { return down; }
	public boolean Left()  { return left; }
	public boolean Right() { return right; }
	public boolean Grab()  { return grab; }
	public boolean Blink() { return blink; }
}
