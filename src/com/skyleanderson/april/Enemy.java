package com.skyleanderson.april;

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;

import com.skyleanderson.april.util.Prop;

public class Enemy extends Prop
{

	public Enemy( )
	{
		// TODO Auto-generated constructor stub
	}

	public Enemy( Color c, float width, float height )
	{
		super(c, width, height);
		// TODO Auto-generated constructor stub
	}

	public Enemy( ArrayList<Animation> animations )
	{
		super(animations);
		// TODO Auto-generated constructor stub
	}

	public Enemy( Animation animation )
	{
		super(animation);
		// TODO Auto-generated constructor stub
	}

	public Enemy( Color c, float width, float height, ArrayList<Animation> animations )
	{
		super(c, width, height, animations);
		// TODO Auto-generated constructor stub
	}

	public Enemy( Color c, float width, float height, Animation animation )
	{
		super(c, width, height, animation);
		// TODO Auto-generated constructor stub
	}

	protected boolean isActive = true;
	protected boolean killed   = false;
	protected boolean handled  = false;

	public void init( GameContainer gc, Game game )
	{
		this.setOffGround();
		this.boundingBox.fill = true;
	}

	public void kill( )
	{
		this.killed = true;
	}

	public boolean isKilled( )
	{
		return this.killed;
	}

	public boolean isHandled( )
	{
		return handled;
	}

	public void handle( )
	{
		this.handled = true;
	}

}
