package com.skyleanderson.april;

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class CallOut extends Particle
{

	public CallOut( )
	{
		// TODO Auto-generated constructor stub
	}

	public CallOut( Color c, float width, float height )
	{
		super(c, width, height);
		// TODO Auto-generated constructor stub
	}

	public CallOut( ArrayList<Animation> animations )
	{
		super(animations);
		// TODO Auto-generated constructor stub
	}

	public CallOut( Animation animation )
	{
		super(animation);
		// TODO Auto-generated constructor stub
	}

	public CallOut( Color c, float width, float height, ArrayList<Animation> animations )
	{
		super(c, width, height, animations);
		// TODO Auto-generated constructor stub
	}

	public CallOut( Color c, float width, float height, Animation animation )
	{
		super(c, width, height, animation);
		// TODO Auto-generated constructor stub
	}

	protected Font _font;
	protected int  _amount;

	public void setFont( Font f )
	{
		_font = f;
	}

	public void setAmount( int amount )
	{
		_amount = amount;
	}

	@Override
	public void init( )
	{
		super.init();
		this.velocity.set(0, -.5f);
		this.activate(DEFAULT_LIFE);
	}

	@Override
	public void render( GameContainer gc, Game game, Graphics g, float offsetX, float offsetY )
	{
		if (_life <= 0) return;

		g.setColor(_color);
		g.setFont(_font);
		g.drawString(String.format("X %02d", _amount), this.left(), this.top());

	}

	@Override
	public void setColor( Color c )
	{
		this._color.r = c.r;
		this._color.g = c.g;
		this._color.b = c.b;
		this._color.a = .5f;
	}

}
