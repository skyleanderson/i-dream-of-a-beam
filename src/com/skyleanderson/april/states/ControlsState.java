package com.skyleanderson.april.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.skyleanderson.april.SoundEffectController;

public class ControlsState extends BasicGameState
{
	public static final int ID = 1;

	public ControlsState( )
	{
		// TODO Auto-generated constructor stub
	}

	private boolean     _return = false;

	private UnicodeFont _fontLarge;
	private UnicodeFont _fontSmall;
	private Sound       _blipSound;

	@Override
	public void init( GameContainer gc, StateBasedGame game ) throws SlickException
	{
		try
		{
			_blipSound = new Sound("res/blip2.wav");
			_fontLarge = new UnicodeFont("res/LILLIPUT.TTF", 32, true, false);
			_fontLarge.addAsciiGlyphs();
			_fontLarge.getEffects().add(new ColorEffect());
			_fontLarge.loadGlyphs();
			_fontSmall = new UnicodeFont("res/LILLIPUT.TTF", 16, true, false);
			_fontSmall.addAsciiGlyphs();
			_fontSmall.getEffects().add(new ColorEffect());
			_fontSmall.loadGlyphs();
		} catch (Exception ex)
		{
			ex.getStackTrace();
		}
	}

	@Override
	public void render( GameContainer gc, StateBasedGame game, Graphics g ) throws SlickException
	{
		g.setFont(_fontLarge);
		g.setColor(Color.white);
		g.drawString("KEYBOARD", 50, 20);
		g.drawString("XBOX CONTROLLER", 50, 180);
		g.drawString("OBJECTIVES", 50, 360);

		g.setFont(_fontSmall);
		// KEYBOARD CONTROLS
		g.drawString("MOVEMENT", 70, 80);
		g.drawString(": ARROW KEYS", 400, 80);
		g.drawString("OR  WASD", 620, 80);

		g.drawString("SHOOT HORIZONTALLY", 70, 100);
		g.drawString(": Z", 400, 100);
		g.drawString("OR  [.]", 620, 100);

		g.drawString("SHOOT VERTICALLY", 70, 120);
		g.drawString(": X", 400, 120);
		g.drawString("OR  [,]", 620, 120);

		// XBOX CONTROLS
		g.drawString("MOVEMENT", 70, 240);
		g.drawString(": L-STICK", 400, 240);
		g.drawString("OR  D-PAD", 620, 240);

		g.drawString("SHOOT HORIZONTALLY", 70, 260);
		g.drawString(": (X)", 400, 260);
		g.drawString("OR  (B)", 620, 260);

		g.drawString("SHOOT VERTICALLY", 70, 280);
		g.drawString(": (Y)", 400, 280);
		g.drawString("OR  (A)", 620, 280);

		// OBJECTIVES
		g.drawString("ENEMIES ABOUND! MINIMIZE THEIR EXISTENCE AND ", 70, 420);
		g.drawString("TURN THEM INTO POTATO FODDER!", 70, 440);
		g.drawString("SHOOT MANY AT ONCE FOR MORE POINTS!!", 70, 460);
		g.drawString("DON'T GET TOUCHED, THEY'RE DEADLY..", 70, 480);

	}

	@Override
	public void update( GameContainer gc, StateBasedGame game, int delta ) throws SlickException
	{
		if (_return)
		{
			_blipSound.play(SoundEffectController.LOWER_FIFTH_FREQUENCY_RATIO, 1.0f);
			game.enterState(StartMenuState.ID);
		}

	}

	@Override
	public int getID( )
	{
		return ID;
	}

	@Override
	public void enter( GameContainer gc, StateBasedGame game )
	{
		_return = false;
	}

	@Override
	public void controllerButtonPressed( int controller, int button )
	{
		_return = true;
	}

	@Override
	public void keyReleased( int key, char c )
	{
		_return = true;

	}
}
