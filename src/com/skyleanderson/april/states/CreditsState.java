package com.skyleanderson.april.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.skyleanderson.april.LD26;
import com.skyleanderson.april.SoundEffectController;

public class CreditsState extends BasicGameState
{
	public static final int ID = 2;

	public CreditsState( )
	{
		// TODO Auto-generated constructor stub
	}

	private boolean     _return = false;

	private UnicodeFont _fontLarge;
	private UnicodeFont _fontSmall;
	private Sound       _blipSound;

	@Override
	public void init( GameContainer gc, StateBasedGame game ) throws SlickException
	{
		try
		{
			_blipSound = new Sound("res/blip2.wav");
			_fontLarge = new UnicodeFont("res/LILLIPUT.TTF", 32, true, false);
			_fontLarge.addAsciiGlyphs();
			_fontLarge.getEffects().add(new ColorEffect());
			_fontLarge.loadGlyphs();
			_fontSmall = new UnicodeFont("res/LILLIPUT.TTF", 16, true, false);
			_fontSmall.addAsciiGlyphs();
			_fontSmall.getEffects().add(new ColorEffect());
			_fontSmall.loadGlyphs();
		} catch (Exception ex)
		{
			ex.getStackTrace();
		}
	}

	@Override
	public void render( GameContainer gc, StateBasedGame game, Graphics g ) throws SlickException
	{
		g.setFont(_fontLarge);
		g.setColor(Color.white);
		g.drawString(LD26.TITLE, 50, 50);
		g.setFont(_fontSmall);

		g.drawString("by KYLE ANDERSON", 50, 100);
		g.drawString("LUDUM DARE 26, APRIL 2013  ", 50, 120);

		g.drawString("MADE WITH:", 50, 180);
		g.drawString("JAVA + ECLIPSE", 70, 200);
		g.drawString("SLICK", 70, 220);
		g.drawString("INUDGE", 70, 240);
		g.drawString("AUDACITY", 70, 260);
		g.drawString("FONT - Lilliput Steps", 70, 280);
		g.drawString("(1001freefonts.com)", 90, 300);

		g.drawString("SPECIAL THANKS:", 50, 340);
		g.drawString("SAM - FOR LETTING ME BOUNCE IDEAS", 70, 360);
		g.drawString("MEGHAN - FOR KEEPING ME FED", 70, 380);

		g.drawString("THANKS FOR PLAYING!", 50, 440);
	}

	@Override
	public void update( GameContainer gc, StateBasedGame game, int delta ) throws SlickException
	{
		if (_return)
		{
			_blipSound.play(SoundEffectController.LOWER_FIFTH_FREQUENCY_RATIO, 1.0f);
			game.enterState(StartMenuState.ID);
		}
	}

	@Override
	public int getID( )
	{
		return ID;
	}

	@Override
	public void enter( GameContainer gc, StateBasedGame game )
	{
		_return = false;
	}

	@Override
	public void controllerButtonPressed( int controller, int button )
	{
		_return = true;
	}

	@Override
	public void keyReleased( int key, char c )
	{
		_return = true;

	}

}
