package com.skyleanderson.april.states;

import java.util.LinkedList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.skyleanderson.april.Enemy;
import com.skyleanderson.april.FollowingEnemy;
import com.skyleanderson.april.LD26;
import com.skyleanderson.april.LinearEnemy;
import com.skyleanderson.april.Particle;
import com.skyleanderson.april.Player;
import com.skyleanderson.april.SinEnemy;
import com.skyleanderson.april.SoundEffectController;
import com.skyleanderson.april.util.Prop;

public class GamePlayState extends BasicGameState
{

	// /////////////////////////////////////////////////////////////////////
	// CONSTANTS ///////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////
	public static final int       ID                   = 10;
	public static final int       TARGET_MILLIS        = 16;                                      // ~ 60fps

	// /////////////////////////////////////////////////////////////////////
	// MEMBER VARIABLES ////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////
	protected int                 _elapsedMillis       = 0;

	protected Player              _player              = new Player(Player.DEFAULT_COLOR, 16, 16);
	protected LinkedList<Enemy>   _enemyList           = new LinkedList<Enemy>();
	protected LinkedList<Enemy>   _removeList          = new LinkedList<Enemy>();

	private boolean               _controllerReady     = false;

	private Sound                 _blipSound;
	private SoundEffectController _soundController     = new SoundEffectController();

	private UnicodeFont           _fontLarge;
	private UnicodeFont           _fontSmall;

	private int                   _difficulty          = 1;

	private long                  _score               = 0;
	private long                  _combo               = 0;
	private int                   _multiplier          = 1;

	private Color                 _scoreColor          = new Color(1.0f, .2f, .2f, .5f);

	private Color                 _backgroundColor     = new Color(.2f, .2f, .2f, .2f);
	private Particle[]            _backGroundParticles = new Particle[5];

	public GamePlayState( )
	{}

	// /////////////////////////////////////////////////////////////////////
	// GAME OVERRIDES //////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////
	@Override
	public void init( GameContainer gc, StateBasedGame game ) throws SlickException
	{
		// load resources
		try
		{
			_blipSound = new Sound("res/blip.wav");
			_fontLarge = new UnicodeFont("res/LILLIPUT.TTF", 32, true, false);
			_fontLarge.addAsciiGlyphs();
			_fontLarge.getEffects().add(new ColorEffect());
			_fontLarge.loadGlyphs();
			_fontSmall = new UnicodeFont("res/LILLIPUT.TTF", 16, true, false);
			_fontSmall.addAsciiGlyphs();
			_fontSmall.getEffects().add(new ColorEffect());
			_fontSmall.loadGlyphs();
		} catch (Exception ex)
		{
			ex.getStackTrace();
		}

		for (int i = 0; i < 5; i++)
		{
			int width = (int) (Math.random() * 400) + 400;
			int height = (int) (Math.random() * 300) + 300;

			int x = (int) (Math.random() * (800 - width));
			int y = (int) (Math.random() * (600 - height));

			_backGroundParticles[i] = new Particle(_backgroundColor, width, height);
			_backGroundParticles[i].setPosition(x, y);
			_backGroundParticles[i].setVelocity(.01f * (float) (Math.random()) * i, .01f * (float) (Math.random()) * i);
			_backGroundParticles[i].setColor(_backgroundColor);
		}

		_soundController.init(gc, game);

		gc.setShowFPS(false);
		gc.getInput().initControllers();
		gc.getInput().clearControlPressedRecord();
		// TODO load all resources and do other initialization
		_player.init(gc, game);
		_player.setPosition(392, 292);
		_player.setSoundEffectController(_soundController);

		spawnInitialEnemies(gc, game);
	}

	@Override
	public void render( GameContainer gc, StateBasedGame game, Graphics g ) throws SlickException
	{
		g.setBackground(Color.black);
		g.clear();
		for (int i = 0; i < 5; i++)
			_backGroundParticles[i].render(gc, game, g, 0, 0);

		// TODO draw stuff
		_player.render(gc, game, g, 0, 0);

		for (Prop enemy : _enemyList)
			enemy.render(gc, game, g, 0, 0);

		// UI
		g.setFont(_fontLarge);
		g.setColor(_scoreColor);
		g.drawString(String.format("%07d", _score), 20, 20);
		g.drawString(String.format("%03d", _combo), 700, 20);

	}

	@Override
	public void update( GameContainer gc, StateBasedGame game, int delta ) throws SlickException
	{
		// TODO real updates!
		// update sound controller outside of frame limiting for better time-synced sound
		_soundController.update(gc, game, delta);

		_elapsedMillis += delta;
		if (_elapsedMillis >= TARGET_MILLIS)
		{
			_elapsedMillis -= TARGET_MILLIS;
			// handle framerate limited update

			if (_soundController.getFramesDone() > 0)
			{
				for (int i = 0; i < (_difficulty / 5) + 1; i++)
					spawnRandomEnemySet(gc, game);

				if (_difficulty % 5 == 4) spawnEnemySet(gc, game, 4);

				_difficulty++;
				_soundController.clearFrames(1);
				_score += 1000;
			}

			if (_controllerReady) pollControllers(gc);

			_player.setEnemyList(_enemyList);
			_player.update();

			_removeList.clear();
			for (Enemy enemy : _enemyList)
			{
				if (!enemy.isKilled())
					enemy.update();
				else
					_removeList.add(enemy);
			}
			for (Enemy e : _removeList)
				_enemyList.remove(e);

			updateBackground();

			// spawn more enemies
			if (_enemyList.isEmpty()) spawnRandomEnemySet(gc, game);

			if (_player.isDead() && _player.readyToContinue()) game.enterState(StartMenuState.ID);
		}

		if (_score > ((LD26) game).highscore)
		{
			((LD26) game).highscore = _score;
		}
		((LD26) game).lastscore = _score;

	}

	// /////////////////////////////////////////////////////////////////////
	// INPUT ///////////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////
	@Override
	public void keyPressed( int key, char c )
	{
		_controllerReady = false;
		// TODO handle key presses
		if (key == Input.KEY_UP || key == Input.KEY_W)
			_player.moveUp();
		else if (key == Input.KEY_DOWN || key == Input.KEY_S)
			_player.moveDown();
		else if (key == Input.KEY_RIGHT || key == Input.KEY_D)
			_player.moveRight();
		else if (key == Input.KEY_LEFT || key == Input.KEY_A) _player.moveLeft();

		if (key == Input.KEY_X || key == Input.KEY_PERIOD)
			_player.attackHorizontal();
		else if (key == Input.KEY_Z || key == Input.KEY_COMMA) _player.attackVertical();

	}

	@Override
	public void keyReleased( int key, char c )
	{
		_controllerReady = false;
		// TODO handle key releases
		if (key == Input.KEY_UP || key == Input.KEY_W)
			_player.stopUp();
		else if (key == Input.KEY_DOWN || key == Input.KEY_S)
			_player.stopDown();
		else if (key == Input.KEY_RIGHT || key == Input.KEY_D)
			_player.stopRight();
		else if (key == Input.KEY_LEFT || key == Input.KEY_A)
			_player.stopLeft();
		else if (key == Input.KEY_1)
			_soundController.stopAll();
		else if (key == Input.KEY_2)
			_soundController.resume();
		else if (key == Input.KEY_ESCAPE) _player.die();

	}

	private void pollControllers( GameContainer gc )
	{
		boolean up = false, down = false, left = false, right = false, horizontal = false, vertical = false;
		for (int c = 0; c < gc.getInput().getControllerCount(); c++)
		{
			if (gc.getInput().isControllerDown(c))
				down = true;
			else if (gc.getInput().isControllerUp(c)) up = true;

			if (gc.getInput().isControllerLeft(c))
				left = true;
			else if (gc.getInput().isControllerRight(c)) right = true;

			if (gc.getInput().isControlPressed(4, c) || gc.getInput().isControlPressed(7, c))
				vertical = true;
			else if (gc.getInput().isControlPressed(5, c) || gc.getInput().isControlPressed(6, c))
				horizontal = true;
			else if (gc.getInput().isControlPressed(10, c)) _player.die();

		}

		if (up)
		{
			_player.moveUp();
			_player.stopDown();
		} else if (down)
		{
			_player.moveDown();
			_player.stopUp();
		} else
		{
			_player.stopDown();
			_player.stopUp();
		}

		if (left)
		{
			_player.moveLeft();
			_player.stopRight();
		} else if (right)
		{
			_player.moveRight();
			_player.stopLeft();
		} else
		{
			_player.stopLeft();
			_player.stopRight();
		}

		if (horizontal)
			_player.attackHorizontal();
		else if (vertical) _player.attackVertical();

	}

	@Override
	public void controllerUpReleased( int controller )
	{
		_controllerReady = true;
	}

	@Override
	public void controllerLeftReleased( int controller )
	{
		_controllerReady = true;
	}

	@Override
	public void controllerDownPressed( int controller )
	{
		_controllerReady = true;
	}

	@Override
	public void controllerRightPressed( int controller )
	{
		_controllerReady = true;
	}

	@Override
	public void controllerButtonPressed( int controller, int button )
	{
		_controllerReady = true;
	}

	// /////////////////////////////////////////////////////////////////////
	// STATE TRANSITION ////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////
	@Override
	public void enter( GameContainer gc, StateBasedGame game )
	{
		_player.init(gc, game);
		_player.setPosition(392, 292);
		_player.setSoundEffectController(_soundController);
		gc.getInput().clearControlPressedRecord();
		_soundController.resume();
		_enemyList.clear();
		spawnInitialEnemies(gc, game);
		_score = 0;
		_combo = 0;
		_multiplier = 1;
		_difficulty = 0;
	}

	@Override
	public void leave( GameContainer gc, StateBasedGame game )
	{
		_soundController.stopAll();
	}

	// /////////////////////////////////////////////////////////////////////
	// HELPERS /////////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////

	@Override
	public int getID( )
	{
		return ID;
	}

	private void spawnInitialEnemies( GameContainer gc, Game game )
	{

		// enemies
		// spawnEnemySet(gc, game, 0);

		// spawnEnemySet(gc, game, 1);

		// spawnEnemySet(gc, game, 2);

		spawnEnemySet(gc, game, 3);
	}

	public void updateBackground( )
	{

		for (Particle p : _backGroundParticles)
		{
			p.update();
			p.activate();
			if (p.left() < 0 || p.right() > 800) p.setVelocityX(p.getVelocityX() * -1);
			if (p.top() < 0 || p.bottom() > 600) p.setVelocityY(p.getVelocityY() * -1);
		}

	}

	private void spawnRandomEnemySet( GameContainer gc, Game game )
	{
		int type = (int) (Math.random() * (3 + (_difficulty / 5)));
		spawnEnemySet(gc, game, type);
	}

	private void spawnEnemySet( GameContainer gc, Game game, int type )
	{
		// pick a quadrant that the player is not in
		int playerQuadrant;
		if (_player.left() < LD26.GAME_WIDTH / 2)
		{
			if (_player.top() < LD26.GAME_HEIGHT / 2)
				playerQuadrant = 0;
			else
				playerQuadrant = 2;
		} else
		{
			if (_player.top() < LD26.GAME_HEIGHT / 2)
				playerQuadrant = 1;
			else
				playerQuadrant = 3;
		}

		int quadrant = (int) (Math.random() * 4);
		if (quadrant == playerQuadrant)
		{
			quadrant += 1;
			quadrant += (int) (Math.random() * 2);
			quadrant %= 4;
		}

		float xmin = 0, xmax = 0, ymin = 0, ymax = 0;
		switch (quadrant)
		{
			case 0:
				xmin = 50;
				ymin = 50;
				xmax = 350;
				ymax = 250;
				break;
			case 1:
				xmin = 450;
				ymin = 50;
				xmax = 750;
				ymax = 250;
				break;
			case 2:
				xmin = 50;
				ymin = 350;
				xmax = 350;
				ymax = 550;
				break;
			case 3:
				xmin = 450;
				ymin = 350;
				xmax = 750;
				ymax = 550;
				break;
		}

		double randomDirection = Math.random();
		switch (type)
		{
			case 0: // vertical movers
				for (int i = 0; i < 5; i++)
				{
					LinearEnemy enemy = new LinearEnemy(new Color(72, 0, 255), 16, 16);
					enemy.init(gc, game);
					enemy.setPosition(xmin + 30 * i, ymin + (int) (randomDirection * (ymax - ymin)));
					if (randomDirection < .5)
						enemy.setVelocity(0, -.75f * (i + 1));
					else
						enemy.setVelocity(0, .75f * (i + 1));

					_enemyList.add(enemy);
				}
				break;
			case 1: // horizontal movers
				for (int i = 0; i < 5; i++)
				{
					LinearEnemy enemy = new LinearEnemy(new Color(172, 0, 255), 16, 16);
					enemy.init(gc, game);
					enemy.setPosition(xmin + (int) (randomDirection * (xmax - xmin)), ymin + 30 * i);
					if (randomDirection < .5)
						enemy.setVelocity(-.75f * (i + 1), 0);
					else
						enemy.setVelocity(.75f * (i + 1), 0);
					_enemyList.add(enemy);
				}
				break;
			case 2: // diagonal movers
				for (int i = 0; i < 3; i++)
				{
					LinearEnemy enemy = new LinearEnemy(new Color(0, 255, 0), 24, 24);
					enemy.init(gc, game);
					enemy.setPosition(xmin + 10 + 20 * i, ymin + 10 + 20 * i);
					if (randomDirection < .5)
						enemy.setVelocity(.66f, -.66f);
					else
						enemy.setVelocity(-.66f, -.66f);

					_enemyList.add(enemy);
				}
				break;
			case 3: // sin enemies
				for (int i = 0; i < 3; i++)
				{
					SinEnemy enemy = new SinEnemy(new Color(40, 80, 255), 12, 12);
					enemy.init(gc, game);
					enemy.setPosition(xmin + 10 + 20 * i, ymin + 10 + 20 * i);
					enemy.setDirection((float) (Math.random()), (float) (Math.random()));

					_enemyList.add(enemy);
				}
				break;
			default: // follower
				for (int i = 0; i < 4; i++)
				{
					FollowingEnemy enemy = new FollowingEnemy(new Color(240, 65, 10), 8, 8);
					enemy.init(gc, game);
					enemy.setPlayer(_player);
					enemy.setPosition((float) (Math.random() * (xmax - xmin)) + xmin, (float) (Math.random() * (ymax - ymin)) + ymin);
					if (randomDirection < .5)
						enemy.setVelocity(.66f, -.66f);
					else
						enemy.setVelocity(-.66f, -.66f);

					_enemyList.add(enemy);
				}
				break;
		}
	}

	public void scoreKills( int count )
	{
		_multiplier = (count);
		for (int i = 1; i <= count; i++)
		{
			_score += (_combo + i) * _multiplier;
		}
		_combo += count;
	}

	public void breakCombo( )
	{
		_combo = 0;
	}
}
