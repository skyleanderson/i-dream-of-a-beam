package com.skyleanderson.april.states;

import java.util.LinkedList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.skyleanderson.april.LD26;
import com.skyleanderson.april.Particle;
import com.skyleanderson.april.util.Prop;

public class StartMenuState extends BasicGameState
{
	public static final int  ID                 = 0;
	private static final int SELECTION_COOLDOWN = 30;

	public StartMenuState( )
	{}

	private UnicodeFont          _fontLarge;
	private UnicodeFont          _fontSmall;
	private Sound                _blipSound;

	private Prop                 _cursor            = new Prop(Color.yellow, 16, 16);
	private Vector2f             _cursorDestination = new Vector2f();
	private int                  _selectedIndex     = 0;
	private boolean              _controllerReady   = false;
	private int                  _elapsedMillis;

	private boolean              _selectionChosen   = false;
	private LinkedList<Particle> _particles         = new LinkedList<Particle>();

	private int                  _selectionCooldown = 0;
	private float                _alpha             = 1.0f;
	private Color                _selectionColor    = new Color(0, 0, 0);

	@Override
	public void init( GameContainer gc, StateBasedGame game ) throws SlickException
	{
		try
		{
			_blipSound = new Sound("res/blip2.wav");
			_fontLarge = new UnicodeFont("res/LILLIPUT.TTF", 32, true, false);
			_fontLarge.addAsciiGlyphs();
			_fontLarge.getEffects().add(new ColorEffect());
			_fontLarge.loadGlyphs();
			_fontSmall = new UnicodeFont("res/LILLIPUT.TTF", 16, true, false);
			_fontSmall.addAsciiGlyphs();
			_fontSmall.getEffects().add(new ColorEffect());
			_fontSmall.loadGlyphs();
		} catch (Exception ex)
		{
			ex.getStackTrace();
		}
		_selectedIndex = 0;
		_cursor.setPosition(200, 304);
	}

	@Override
	public void render( GameContainer gc, StateBasedGame game, Graphics g ) throws SlickException
	{
		// draw selection effect
		if (_selectionCooldown > 0)
		{
			_selectionColor.r = Color.yellow.r;
			_selectionColor.g = Color.yellow.g;
			_selectionColor.b = Color.yellow.b;
			_selectionColor.a = _alpha;
			g.setColor(_selectionColor);

			g.fillRect(_cursor.left() - 8, _cursor.top() - 8, _cursor.boundingBox.getWidth() + 2 * 8, _cursor.boundingBox.getHeight() + 2 * 8);

			g.fillRect(0, _cursor.top(), 192, 16); // left bar
			g.fillRect(224, _cursor.top(), 776, 16);
			g.setColor(Color.yellow);
			g.fillRect(0, _cursor.top() + 4, 192, 8);
			g.fillRect(224, _cursor.top() + 4, 776, 8);
		}

		g.setFont(_fontLarge);
		g.setColor(Color.white);
		g.drawString(LD26.TITLE, 50, 50);

		g.setFont(_fontSmall);
		g.drawString("PLAY", 400, 300);
		g.drawString("CONTROLS", 400, 350);
		g.drawString("CREDITS", 400, 400);

		if (((LD26) game).highscore > 0)
		{
			g.setColor(Color.lightGray);
			int titleWidth = _fontLarge.getWidth(LD26.TITLE);
			int scoreWidth = _fontSmall.getWidth(String.format("HIGH: %07d", ((LD26) game).highscore));
			int x = 50 + titleWidth - scoreWidth;
			g.drawString(String.format("LAST: %07d", ((LD26) game).lastscore), x, 90);
			g.drawString(String.format("HIGH: %07d", ((LD26) game).highscore), x, 110);
		}

		_cursor.render(gc, game, g);

	}

	@Override
	public void update( GameContainer gc, StateBasedGame game, int delta ) throws SlickException
	{
		if (!_selectionChosen && _controllerReady) pollControllers(gc);

		_elapsedMillis += delta;
		if (_elapsedMillis >= GamePlayState.TARGET_MILLIS)
		{
			_elapsedMillis -= GamePlayState.TARGET_MILLIS;
			// handle framerate limited update

			_cursorDestination.set(200, 304 + (_selectedIndex * 50));
			if (_cursor.getPosition().y < _cursorDestination.y)
				_cursor.setVelocity(0, 5);
			else if (_cursor.getPosition().y > _cursorDestination.y)
				_cursor.setVelocity(0, -5);
			else
				_cursor.setVelocity(0, 0);

			_cursor.update();

			if (_selectionCooldown > 0)
			{
				_selectionCooldown--;
				_alpha = (float) ((float) (_selectionCooldown) / (float) (SELECTION_COOLDOWN));
			}

			if (_selectionCooldown == 8) _blipSound.play();

			if (_selectionCooldown == 0 && _particles.size() == 0 && _selectionChosen)
			{
				// change states
				if (_selectedIndex == 0) // play
					game.enterState(GamePlayState.ID);
				else if (_selectedIndex == 1) // controls
				{
					game.enterState(ControlsState.ID);
				} else if (_selectedIndex == 2) // credits
				{
					game.enterState(CreditsState.ID);
				}
			}
		}
	}

	@Override
	public int getID( )
	{
		return ID;
	}

	@Override
	public void keyReleased( int key, char c )
	{
		if (_selectionChosen) return;
		// TODO handle key releases
		if (key == Input.KEY_UP || key == Input.KEY_W)
			_selectedIndex--;
		else if (key == Input.KEY_DOWN || key == Input.KEY_S)
			_selectedIndex++;
		else if (key == Input.KEY_X || key == Input.KEY_Z || key == Input.KEY_COMMA || key == Input.KEY_PERIOD || key == Input.KEY_ENTER)
		{
			_selectionChosen = true;
			_selectionCooldown = SELECTION_COOLDOWN;
		}

		if (_selectedIndex < 0)
			_selectedIndex = 0;
		else if (_selectedIndex > 2) _selectedIndex = 2;

	}

	private boolean _downPressed = false;
	private boolean _upPressed   = false;

	private void pollControllers( GameContainer gc )
	{
		for (int c = 0; c < gc.getInput().getControllerCount(); c++)
		{
			if (gc.getInput().isControllerDown(c) && !_downPressed)
			{
				_selectedIndex++;
				_downPressed = true;
			} else if (gc.getInput().isControllerUp(c) && !_upPressed)
			{
				_selectedIndex--;
				_upPressed = true;
			}

			if (gc.getInput().isControlPressed(4, c) || gc.getInput().isControlPressed(7, c) || gc.getInput().isControlPressed(5, c)
			        || gc.getInput().isControlPressed(6, c))
			{
				_selectionChosen = true;
				_selectionCooldown = SELECTION_COOLDOWN;
			}

		}

		if (_selectedIndex < 0)
			_selectedIndex = 0;
		else if (_selectedIndex > 2) _selectedIndex = 2;
	}

	@Override
	public void controllerUpReleased( int controller )
	{
		_controllerReady = true;
		_upPressed = false;
	}

	@Override
	public void controllerLeftReleased( int controller )
	{
		_controllerReady = true;
	}

	@Override
	public void controllerDownPressed( int controller )
	{
		_controllerReady = true;
	}

	@Override
	public void controllerDownReleased( int controller )
	{
		_controllerReady = true;
		_downPressed = false;
	}

	@Override
	public void controllerRightPressed( int controller )
	{
		_controllerReady = true;
	}

	@Override
	public void controllerButtonPressed( int controller, int button )
	{
		_controllerReady = true;
	}

	@Override
	public void enter( GameContainer gc, StateBasedGame game )
	{
		_selectionChosen = false;
		_selectedIndex = 0;
		_selectionCooldown = 0;
		_controllerReady = false;
		_upPressed = false;
		_downPressed = false;
		_cursor.setPosition(200, 304);
		gc.getInput().clearControlPressedRecord();
	}

}
