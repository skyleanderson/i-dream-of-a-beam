package com.skyleanderson.april;

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

public class FollowingEnemy extends Enemy
{
	public static final float MAX_SPEED      = 3.2f;
	public static final float MIN_SPEED      = 1.1f;
	public static final float PI_OVER_4      = 45;  // 0.7854f;
	public static final float PI_OVER_2      = 90;  // 1.571f;
	public static final int   FRAMES_TO_TURN = 15;

	public FollowingEnemy( )
	{
		// TODO Auto-generated constructor stub
	}

	public FollowingEnemy( Color c, float width, float height )
	{
		super(c, width, height);
		// TODO Auto-generated constructor stub
	}

	public FollowingEnemy( ArrayList<Animation> animations )
	{
		super(animations);
		// TODO Auto-generated constructor stub
	}

	public FollowingEnemy( Animation animation )
	{
		super(animation);
		// TODO Auto-generated constructor stub
	}

	public FollowingEnemy( Color c, float width, float height, ArrayList<Animation> animations )
	{
		super(c, width, height, animations);
		// TODO Auto-generated constructor stub
	}

	public FollowingEnemy( Color c, float width, float height, Animation animation )
	{
		super(c, width, height, animation);
		// TODO Auto-generated constructor stub
	}

	private Player   _player     = null;
	private Vector2f _tempVector = new Vector2f();
	private int      _frames     = 0;
	private float    _rotation   = 0;

	@Override
	public void render( GameContainer gc, Game game, Graphics g, float offsetX, float offsetY )
	{
		if (!killed)
		{
			g.setColor(this.boundingBox.color);
			g.fillRect(this.left(), this.top(), this.right() - this.left(), this.bottom() - this.top());

		}
	}

	@Override
	public void update( )
	{
		if (killed) return;

		_frames++;
		// accelerate towards player
		_tempVector.set(_player.getPosition());
		_tempVector.sub(position);
		_tempVector.normalise();
		_tempVector.scale(1.8f);
		_tempVector.add(_rotation);
		acceleration.set(_tempVector);

		if (_frames == FRAMES_TO_TURN)
		{
			_rotation = (float) (Math.random() * PI_OVER_2) - PI_OVER_4;
		}
		velocity.add(acceleration);

		if (velocity.length() > MAX_SPEED)
		{
			velocity.normalise();
			velocity.scale(MAX_SPEED);
		} else if (velocity.length() < MIN_SPEED)
		{
			velocity.normalise();
			velocity.scale(MIN_SPEED);
		}

		position.add(velocity);

		if (position.x < 0)
		{
			position.x = 0;
			this.velocity.x = 0;
		} else if (this.right() > LD26.GAME_WIDTH)
		{
			position.x = LD26.GAME_WIDTH - this.boundingBox.getWidth();
			this.velocity.x = 0;
		}

		if (position.y < 0)
		{
			position.y = 0;
			this.velocity.y = 0;
		} else if (this.bottom() > LD26.GAME_HEIGHT)
		{
			position.y = LD26.GAME_HEIGHT - this.boundingBox.getHeight();
			this.velocity.y = 0;
		}
	}

	public void setPlayer( Player p )
	{
		_player = p;
	}
}
