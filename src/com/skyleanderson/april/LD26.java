package com.skyleanderson.april;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.skyleanderson.april.states.ControlsState;
import com.skyleanderson.april.states.CreditsState;
import com.skyleanderson.april.states.GamePlayState;
import com.skyleanderson.april.states.StartMenuState;

public class LD26 extends StateBasedGame
{

	public static final String TITLE       = "I DREAM OF A BEAM";

	public static final int    GAME_WIDTH  = 800;
	public static final int    GAME_HEIGHT = 600;

	public long                highscore   = 0;
	public long                lastscore   = 0;

	public LD26( String title )
	{
		super(TITLE);
	}

	public LD26( )
	{
		super(TITLE);
	}

	@Override
	public void initStatesList( GameContainer gc ) throws SlickException
	{
		this.addState(new StartMenuState());
		this.addState(new GamePlayState());
		this.addState(new ControlsState());
		this.addState(new CreditsState());
		// this.addState(new PauseState());
		// this.addState(new EndGameState());
	}

	/**
	 * @param args
	 */
	public static void main( String[] args )
	{
		try
		{

			AppGameContainer container = new AppGameContainer(new LD26(TITLE));
			container.setDisplayMode(GAME_WIDTH, GAME_HEIGHT, false);
			container.start();
		} catch (SlickException e)
		{
			e.printStackTrace();
		}
	}

}
