package com.skyleanderson.april;

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.skyleanderson.april.util.Prop;

public class Particle extends Prop
{

	public static final long  DEFAULT_LIFE  = 60;
	public static final float DEFAULT_SPEED = 3;
	public static final float MIN_SIZE      = 2;
	public static final float MAX_SIZE      = 32;
	public static final float DEFAULT_ALPHA = .2f;

	public Particle( )
	{
		// TODO Auto-generated constructor stub
	}

	public Particle( Color c, float width, float height )
	{
		super(c, width, height);
		// TODO Auto-generated constructor stub
	}

	public Particle( ArrayList<Animation> animations )
	{
		super(animations);
		// TODO Auto-generated constructor stub
	}

	public Particle( Animation animation )
	{
		super(animation);
		// TODO Auto-generated constructor stub
	}

	public Particle( Color c, float width, float height, ArrayList<Animation> animations )
	{
		super(c, width, height, animations);
		// TODO Auto-generated constructor stub
	}

	public Particle( Color c, float width, float height, Animation animation )
	{
		super(c, width, height, animation);
		// TODO Auto-generated constructor stub
	}

	protected long  _life  = DEFAULT_LIFE;
	protected Color _color = new Color(255, 0, 255);

	public void init( )
	{
		this.setOffGround();
		this.boundingBox.fill = true;
		setColor(this.boundingBox.color);
	}

	@Override
	public void render( GameContainer gc, Game game, Graphics g, float offsetX, float offsetY )
	{
		if (_life <= 0) return;

		g.setColor(_color);
		g.fillRect(this.left(), this.top(), this.right() - this.left(), this.bottom() - this.top());

	}

	@Override
	public void update( )
	{
		if (this._life > 0) _life--;
		super.update();
	}

	public void activate( )
	{
		this.activate(DEFAULT_LIFE);
	}

	public void activate( long life )
	{
		this._life = life;
	}

	public boolean isActive( )
	{
		return this._life > 0;
	}

	public void setColor( Color c )
	{
		this._color.r = c.r;
		this._color.g = c.g;
		this._color.b = c.b;
		this._color.a = Math.min(c.a, DEFAULT_ALPHA);
	}

	public void setColor( Color c, float variation )
	{
		float random = (float) (2 * Math.random()) - 1;
		this._color.r += c.r * (variation * random);
		random = (float) (2 * Math.random()) - 1;
		this._color.g += c.g * (variation * random);
		random = (float) (2 * Math.random()) - 1;
		this._color.b += c.b * (variation * random);
		random = (float) (2 * Math.random()) - 1;

		this._color.a = Math.min(c.a, DEFAULT_ALPHA);
	}

	public void setNewSize( float width, float height )
	{
		this.boundingBox.setWidth(width);
		this.boundingBox.setHeight(height);
	}

	public void setNewRandomSize( )
	{
		this.setNewRandomSize(MIN_SIZE, MAX_SIZE);
	}

	public void setNewRandomSize( float min, float max )
	{
		float width = (float) (Math.random());
		width *= max - min;
		width += min;
		// float height = (float) (2f * Math.random()) - 1;
		// height *= max - min;
		float height = width;
		this.setNewSize(width, height);
	}
}
