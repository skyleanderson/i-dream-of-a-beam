package com.skyleanderson.april;

import java.util.LinkedList;

import org.newdawn.slick.Color;

public class ParticleFactory
{
	public static final int DEFAULT_CAPACITY = 600;

	public ParticleFactory( )
	{
		this.setCapacity(DEFAULT_CAPACITY);
	}

	protected LinkedList<Particle> particleList = new LinkedList<Particle>();
	protected int                  current      = -1;
	protected int                  capacity     = DEFAULT_CAPACITY;

	public void setCapacity( int capacity )
	{
		this.capacity = capacity;
		for (int i = 0; i < capacity; i++)
		{
			Particle p;
			p = new Particle(Color.orange, 8, 8);

			particleList.add(p);
		}
	}

	public Particle getNextParticle( )
	{
		current++;
		if (current >= capacity) current = 0;

		Particle p = this.particleList.get(current);
		p.init();
		p.activate();
		return p;
	}

}
